from setuptools import setup

setup(
    name='prettyAlerts',
    version='0.1.0',
    description='Image based alert plugin for nagios and compatible monitoring systems. Supports slack, mattermost, discord, synology chat, etc...',
    url='https://gitlab.com/Jedimaster0/prettyAlerts',
    author='Nathan Snow',
    author_email='admin@mimir-tech.org',
    license='GPLv3',
    packages=['prettyAlerts'],
    install_requires=[
        'selenium',
        'pillow'
    ],
    zip_safe=False
)

