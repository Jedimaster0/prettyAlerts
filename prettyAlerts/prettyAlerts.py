from lib.logger import Logger
from urllib import request, parse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from PIL import Image
from io import BytesIO
from contextlib import closing
import http.server
import socketserver
import threading
import argparse
import json
import os
import re
import socket
import time
import sys


parser = argparse.ArgumentParser()
parser.add_argument("--nagioshost", required=True, default=socket.gethostname(), help="Nagios server host name")
parser.add_argument("--serviceorhost", required=True, choices=['host', 'service'], help="Alert type")
parser.add_argument("--hostalias", required=True, help="$HOSTNAME$")
parser.add_argument("--notificationtype", required=True, help="$NOTIFICATIONTYPE$")

parser.add_argument("--servicestate", help="$SERVICESTATE$")
parser.add_argument("--servicedesc", help="$SERVICEDESC$")
parser.add_argument("--serviceoutput", help="$SERVICEOUTPUT$")
parser.add_argument("--servicenotes", help="$SERVICENOTES$")

parser.add_argument("--hoststate", help="$HOSTSTATE$")
parser.add_argument("--hostoutput", help="$HOSTOUTPUT$")
parser.add_argument("--hostnotes", help="$HOSTNOTES$")

parser.add_argument("--webhookusername", required=False, help="Webhook username")
parser.add_argument("--webhookchannel", required=False, default='#alerts', help="Webhook channel. DEFAULT: #alerts")
parser.add_argument("--webhookurl", required=True, help="Webhook URL")
parser.add_argument("--imgurl", required=False, help="Image icon URL")
parser.add_argument("--platform", required=False, default='slack', choices=['slack', 'mattermost', 'synchat', 'discord'], help="Chat platform type")
parser.add_argument("--loglevel", required=False, default='info', choices=['debug', 'info', 'warning', 'error', 'critical'], help="Log level")
parser.add_argument("--port", required=False, default=8080, help='Webserver port 8080')
parser.add_argument("--address", required=False, default='localhost', help='Webserver bind address. DEFAULT: localhost')
args = parser.parse_args()

logger = Logger(__name__, args.loglevel)

appPath = os.path.dirname(os.path.realpath(__file__))
staticPath = '/tmp'
httproot='http://' + args.address + ':' + str(args.port)
epoch_time = int(time.time())
fn = str(epoch_time) + '.png'
fnPath = os.path.join(staticPath, fn)
logger.info('init: appPath: %s \nhttproot: %s \nepoch_time: %s\nfnPath: %s', appPath, httproot, epoch_time, fnPath)
os.chdir(staticPath)

class WebServer:
  def __init__(self, address, port):
    self.port = port
    self.address = address
    self.handler_class=http.server.SimpleHTTPRequestHandler
    self.server_class=http.server.HTTPServer
  def start(self):
    serverAddress = (self.address, int(self.port))
    self.httpd = self.server_class(serverAddress, self.handler_class)
    thread = threading.Thread(target=self.httpd.serve_forever)
    thread.start()
    logger.info('WebServer: started successfully on: %s:%s', self.address, self.port)
  def stop(self):
    self.httpd.shutdown()
    logger.info('WebServer: stopped successfully')

def getColor():
  color = ""
  if args.servicestate == "CRITICAL":
    color = "#dc322f" #Red
  elif args.servicestate == "WARNING":
    color = "#cb4b16" #Orange
  elif args.servicestate == "UNKNOWN":
    color = "#6c71c4" #Violet
  elif args.servicestate == "OK":
    color = "#859900" #Green
  elif args.hoststate == "DOWN":
    color = "#dc322f" #Red
  elif args.hoststate == "UNREACHABLE":
    color = "#6c71c4" #Violet
  elif args.hoststate == "UP":
    color = "#859900" #Green
  else:
    color = "#b58900" #Yellow
  return color

def getHtml(textOne, textTwo='', textThree=''):
  color = getColor()

  html = """
    <!DOCTYPE html lang="en">
    <head>
      <meta charset="utf-8" />
      <title>Alert!</title>
    </head>
    <html>
      <body>
        <div id="alert" style="background: #002b36; position: fixed; min-width:500px; min-height: 60px; padding: 0.5em;">
          <div style="background: {color}; float: left; margin: 0.25em">
            <img src="{imgUrl}" style="width: 60px; padding: 5px; margin: 0"/>
          </div>
          <div style="color: #93a1a1; background: #073642; min-width: 450px; min-height: 60px; float: left; padding: 0.25em; margin: 0.25em; word-wrap: break-word;">
            <div style="color: {color}">
              {lineOne}
            </div>
            <div>
              {lineTwo}
            </div>
            <div>
              {lineThree}
            </div>
          </div>
        </div>
      </body>
    </html>
  """.format(imgUrl=args.imgurl, color=color, lineOne=textOne, lineTwo=textTwo, lineThree=textThree).strip().replace('\n', '').replace('  ', '')
  logger.debug("getHtml: html created successfully: %s", html)
  return html


def htmlToImg(html):
  try:
    options = Options()
    options.add_argument("--headless")
    options.add_argument("--window-size=800x600")

    with webdriver.Chrome(chrome_options=options) as driver:
      driver.execute_script("document.documentElement.innerHTML = '" + html + "';")
      time.sleep(1.5) #It can take a few seconds to load icons/images
      element = driver.find_element_by_id('alert')
      location = element.location
      size = element.size
      png = driver.get_screenshot_as_png()
      logger.debug('htmlToImg: HTML successfully converted to image')

    with Image.open(BytesIO(png)) as img:
      left = location['x']
      upper = location['y']
      right = location['x'] + size['width']
      lower = location['y'] + size['height']
      img = img.crop((left, upper, right, lower))
      img.save(fnPath, format='png')
      logger.debug('htmlToImg: Image cropped and saved successfully')

  except Exception as e:
    logger.crit('htmlToImg: Unable to create image: %s', e)

def ingestAlert():
  text1 = ''
  text2 = ''
  text3 = ''
  try:
    if bool(re.search('service', args.serviceorhost, re.IGNORECASE)):
      text1 = args.notificationtype + ": " + args.hostalias + " - " + args.servicedesc + " is " + args.servicestate
      text2 = args.serviceoutput
      text3 = "NOTES: " + args.servicenotes
    elif bool(re.search('host', args.serviceorhost, re.IGNORECASE)):
      text1 = args.notificationtype + ": " + args.hostalias + " is " + args.hoststate
      text2 = args.hostoutput
      text3 = args.hostnotes
    logger.debug('ingestAlert: %s', {"one": text1, "two": text2, "three": text3})

    return {"one": text1, "two": text2, "three": text3}
  except Exception as e:
    logger.crit('ingestAlert: Unable to ingest alert: %s', e)

def encodeString(string):
    s = parse.quote(string)
    return s

def sendAlert():
  pl = {}
  fileUrl = os.path.join(httproot, fn)
  color = getColor()

  try:
    if bool(re.search('slack|mattermost|discord', args.platform, re.IGNORECASE)):
      if bool(re.search('service', args.serviceorhost, re.IGNORECASE)):
        titleLink = "http://" + args.nagioshost + "/nagios/cgi-bin/status.cgi?host=" + args.hostalias
        title = args.notificationtype + ": " + args.hostalias + " - " + args.servicedesc + " IS " + args.servicestate
        text = args.serviceoutput
        fallback = title + "\n" + args.serviceoutput

      elif bool(re.search('host', args.serviceorhost, re.IGNORECASE)):
        titleLink = "http://" + args.nagioshost + "/nagios/cgi-bin/status.cgi?host=" + args.hostalias
        title = args.notificationtype + ": " + args.hostalias + " IS " + args.hoststate
        text = args.hostoutput
        fallback = title + "\n" + args.hostoutput

      pl = {
        "channel": args.webhookchannel,
        "username": args.webhookusername,
        "icon_url": args.imgurl,
        "attachments": [
          {
            "fallback": fallback,
            "color": color,
            "text": text,
            "author_name": args.nagioshost,
            "author_icon": args.imgurl,
            "author_link": 'http://' + args.nagioshost + '/nagios',
            "image_url": fileUrl,
            "title": title,
            "title_link": titleLink
          }
        ]
      }
      pl = json.dumps(pl, ensure_ascii=False)

    elif bool(re.search('synchat', args.platform, re.IGNORECASE)):
      if bool(re.search('service', args.serviceorhost, re.IGNORECASE)):
        lineOne = "<http://" + args.nagioshost + "/nagios/cgi-bin/status.cgi?host=" + args.hostalias + "|" + args.nagioshost + ">"
        lineTwo = args.notificationtype + ": " + args.hostalias + " - " + args.servicedesc + " IS " + args.servicestate
        lineThree = args.serviceoutput

      elif bool(re.search('host', args.serviceorhost, re.IGNORECASE)):
        lineOne = "<http://" + args.nagioshost + "/nagios/cgi-bin/status.cgi?host=" + args.hostalias + "|" + args.nagioshost + ">"
        lineTwo = args.notificationtype + ": " + args.hostalias + " IS " + args.hoststate
        lineThree = args.hostoutput

      pl = "payload=" + json.dumps({"text": parse.quote(lineOne + "\n" + lineTwo + "\n" + lineThree), "file_url": fileUrl}, ensure_ascii=False)

    logger.debug('sendAlert: %s', pl)

    req = request.Request(args.webhookurl, data=pl.encode(), headers={'Content-type':'application/json'})
    with request.urlopen(req) as response:
      if response.code == 200:
        logger.info('sendAlert: alert sent to chat platform successfully, response %s', response.code)
        time.sleep(3)
      else:
        logger.err('sendAlert: Unable to send alert to chat platform: response %s', response.code)
  except Exception as e:
    logger.crit('sendAlert: Unable to send alert: %s', e)
  

def main():
  try:
    webserver = WebServer(args.address, args.port)
    text = ingestAlert()
    html = getHtml(text['one'], text['two'], text['three'])
    htmlToImg(html)
    
    while webserver.port < 8089:
      try:
        webserver.start()
        break
      except:
        webserver.port += 1
        logger.info('main: port already in use. Using another port: %s', webserver.port)
    else:
      logger.crit('main: could not start webserver. ports 8080-8089 are in use')

    sendAlert()
  except KeyboardInterrupt:
    webserver.stop()
    pass
  except Exception as e:
    logger.err('main: %s', e)
  finally: #Application cleanup
    try:
      os.remove(fnPath)
      logger.debug('main: local file removed successfully')
    except FileNotFoundError:
      logger.warn("main: Could not cleanup file since it doesn't exist")
    except Exception as e:
      logger.err('main: unable to remove file: %s', e)

    try:
      webserver.stop()
    except Exception as e:
      logger.crit('main: unable to stop webserver: %s', e)

if __name__ == '__main__':
  main()
